# Vyper Journey

Purpose: Learning Vyper smart contract

# Docker

Docker is the container that use for running virtual environment to run Vyper.

# Vyper

[Vyper](https://vyper.readthedocs.io/en/latest/) is a smart contract using Python based languages target to run on Ethereum Virtual Machine (EVM)


## Create Docker

Start with install Docker into local machine. 

> Docker installation link: [Docker.com](https://www.docker.com/)
> Getting start with Docker [Click Here](https://docs.docker.com/docker-for-windows/)

## Build Vyper container

    $docker run -it --entrypoint /bin/bash ethereum/vyper
or

    $docker build https://github.com/vyperlang/vyper.git -t vyper:1
    
## Run Vyper script

    $docker run -v vyperlang/vyper .\<contract_file.vy>
or

    $docker run -v $(pwd):/code vyperlang/vyper /code/<contract_file.vy>
or

    $docker run ethereum/vyper /code/<contract_file.vy>
